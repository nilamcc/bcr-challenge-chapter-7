import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import LandingPage from './pages/LandingPage';
import SearchBar from "./pages/SearchCar";
import { Provider } from "react-redux";
import store from "./redux/store";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
          <Routes>
            <Route path="/" element={<LandingPage />} />
            <Route path="/cars" element={<SearchBar />} />
          </Routes>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
