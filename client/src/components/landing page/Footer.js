import React from 'react';
import {Container} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../assets/css/LandingPage.css';
import list from '../../assets/img/list item.png'
import logo from '../../assets/img/Rectangle 74.png'

export default function Footer() {
    return (
        <Container>
            <div className="row pt-5 ">
                <div className="col-md-3 footer-list" >
                    <ul>
                        <li>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</li>
                        <li>binarcarrental@gmail.com</li>
                        <li>081-233-334-808</li>
                    </ul>
                </div>
                <div className="col-md-3 footer-list">
                    <ul>
                        <li>Our services</li>
                        <li>Why Us</li>
                        <li>Testimonial</li>
                        <li>FAQ</li>
                    </ul>
                </div>
                <div className="col-md-3 footer-list">
                    <ul className="list-media">
                        <li>Connect with us</li>
                        <img src={list} width="220px" height="32px" alt=""/>
                    </ul>
                </div>
                <div className="col-md-3 footer-list" >
                    <ul>
                        <li>Copyright Binar 2022</li>
                        <img src={logo} width="100px" height="34px" alt=""/>
                    </ul>
                </div>
            </div>
        </Container>
    )
}
