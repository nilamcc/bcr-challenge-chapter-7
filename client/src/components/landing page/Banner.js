import React from 'react';
import { Container, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../assets/css/LandingPage.css';

export default function Banner() {
    return (
        <Container>
            <div className="getting-started-section" id="getting-started">
                <div className="banner pt-5">
                    <div className="bg-banner mt-5">
                        <center>
                            <p className="banner-title pt-5">Sewa Mobil di Lampung Sekarang</p>
                            <p className="banner-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <Button variant="success" className="btn mt-5">Mulai Sewa Mobil</Button>
                        </center>
                    </div>
                </div>
            </div>
        </Container>
    )
}