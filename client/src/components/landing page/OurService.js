import React from "react";
import { Container} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../assets/css/LandingPage.css';
import Check from '../../assets/img/Group 53@4x.png';
import Service from '../../assets/img/img_service.png';

export default function OurService() {
    return (
        <Container>
            <div className="our-service-section" id="Our-services">
                <div className="row">
                    <div className="service-pict col-md-6 mt-1 pl-3 pt-3">
                        <img src={Service} className="img-fluid mt-4" width="400px" height="428px" alt=""/>
                    </div>
                    <div className="service-text col-md-6 mt-1 pt-5">
                        <h4>Best Car Rental for any kind of trip in Lampung!</h4>
                        <p>Sewa mobil di Lampung bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                        <div className="service-list">
                            <ul>
                                <li><img src={Check} alt=""/> Sewa Mobil Dengan Supir di Bali 12 Jam</li> <br/>
                                <li><img src={Check} alt=""/> Sewa Mobil Lepas Kunci di Bali 24 Jam</li> <br/>
                                <li><img src={Check} alt=""/> Sewa Mobil Jangka Panjang Bulanan</li> <br/>
                                <li><img src={Check} alt=""/> Gratis Antar - Jemput Mobil di Bandara</li> <br/>
                                <li><img src={Check} alt=""/> Layanan Airport Transfer / Drop In Out</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </Container>
    )
}