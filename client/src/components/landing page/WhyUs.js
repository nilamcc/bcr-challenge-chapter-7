import React from "react";
import { Container, Card } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../assets/css/LandingPage.css';
import complete from '../../assets/img/icon_complete.png';
import hours from '../../assets/img/icon_24hrs.png';
import price from '../../assets/img/icon_price.png';
import professional from '../../assets/img/icon_professional.png';

export default function WhyUs() {
    return (
        <div className="why-us-section" id="Why-Us">
            <Container>
                <div className="why-us-title pb-5">
                    <h4 className="pt-5">Why Us?</h4>
                    <p>Mengapa harus pilih Binar Car Rental?</p>
                </div>
                <div className="card-deck">
                    <div className="row">
                        <div className="col">
                            <Card>
                                <img src={complete} width="32px" height="32px" alt="..." />
                                <Card.Body>
                                    <p className ="title-card">Mobil Lengkap</p>
                                    <Card.Text>Tersedia banyak piliihan mobil, kondisi masih baru, bersih dan terawat</Card.Text>
                                </Card.Body>
                            </Card>
                        </div>
                        <div className="col">
                            <Card>
                                <img src={price} width="32px" height="32px" alt="..." />
                                <Card.Body>
                                    <p className ="title-card">Harga Murah</p>
                                    <Card.Text>Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</Card.Text>
                                </Card.Body>
                            </Card>
                        </div>
                        <div className="col">
                            <Card>
                                <img src={hours} width="32px" height="32px" alt="..." />
                                <Card.Body>
                                    <p className ="title-card">Layanan 24 Jam</p>
                                    <Card.Text>Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</Card.Text>
                                </Card.Body>
                            </Card>
                        </div>
                        <div className="col">
                            <Card>
                                <img src={professional} width="32px" height="32px" alt="..." />
                                <Card.Body>
                                    <p className ="title-card">Sopir Profesional</p>
                                    <Card.Text> Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</Card.Text>
                                </Card.Body>
                            </Card>
                        </div>
                    </div>
                </div>
            </Container>
        </div>
    )
}