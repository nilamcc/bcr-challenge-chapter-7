import React from "react";
import {Container, Navbar, Nav, Button} from 'react-bootstrap';
import logo from "../../assets/img/logo.png";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../assets/css/LandingPage.css';

export default function Navigation() {
    return(
        <Navbar expand="lg" bg='light'>
            <Container>
                <Navbar.Brand href="#" ><img className="logo" src={logo} width="100px" height="34px" alt=""/></Navbar.Brand>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon" />
                </button>
                <Navbar.Collapse>
                    <Nav className="justify-content-end flex-grow-1">
                        <Nav.Link className="nav-link mx-2" href="#Our-services">Our Services</Nav.Link>
                        <Nav.Link className="nav-link mx-2" href="#Why-Us">Why Us</Nav.Link>
                        <Nav.Link className="nav-link mx-2" href="#testimonial">Testimonial</Nav.Link>
                        <Nav.Link className="nav-link mx-2" href="#getting-started">FAQ</Nav.Link>
                        <Button variant="success mx-2">register</Button>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}