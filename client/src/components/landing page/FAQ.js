import React from 'react';
import { Accordion, Container, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../assets/css/LandingPage.css';

export default function faq() {
  return (
    <Container>
      <div className="FAQ-section container pt-5" id="FAQ">
        <div className="FAQ-content row pt-5">
          <div className="FAQ-title col-md-6">
            <h5>Frequently Asked Question</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
          </div>
          <Col md={6}>
            <Accordion className='FAQ-accordion'>
              <Accordion.Item eventKey="0">
                <Accordion.Header>Apa saja syarat yang dibutuhkan?</Accordion.Header>
                <Accordion.Body>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci ipsum, porro et vitae perferendis in odit quod nisi sapiente iste?</Accordion.Body>
              </Accordion.Item>
            </Accordion>
            <Accordion>
              <Accordion.Item eventKey="1">
                <Accordion.Header>Berapa hari minimal mobil sewa lepas kunci?</Accordion.Header>
                <Accordion.Body>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci ipsum, porro et vitae perferendis in odit quod nisi sapiente iste?</Accordion.Body>
              </Accordion.Item>
            </Accordion>
            <Accordion>
              <Accordion.Item eventKey="2">
                <Accordion.Header>Berapa hari sebelumnya sebaiknya booking sewa mobil?</Accordion.Header>
                <Accordion.Body>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci ipsum, porro et vitae perferendis in odit quod nisi sapiente iste?</Accordion.Body>
              </Accordion.Item>
            </Accordion>
            <Accordion>
              <Accordion.Item eventKey="3">
                <Accordion.Header>Apakah ada biaya antar jemput?</Accordion.Header>
                <Accordion.Body>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci ipsum, porro et vitae perferendis in odit quod nisi sapiente iste?</Accordion.Body>
              </Accordion.Item>
            </Accordion>
            <Accordion>
              <Accordion.Item eventKey="4">
                <Accordion.Header>Bagaimana jika terjadi kecelakaan?</Accordion.Header>
                <Accordion.Body>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci ipsum, porro et vitae perferendis in odit quod nisi sapiente iste?</Accordion.Body>
              </Accordion.Item>
            </Accordion>
          </Col>
        </div>
      </div>
    </Container>
  )
}
