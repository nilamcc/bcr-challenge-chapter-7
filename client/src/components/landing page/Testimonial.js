import React from "react";
import '../../assets/css/LandingPage.css';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.min.css';
import 'owl.carousel/dist/assets/owl.theme.default.min.css';
import rate from "../../assets/img/Rate.png";
import photo from "../../assets/img/img_photo.png";
import photo2 from "../../assets/img/img_photo(1).png";
import right from "../../assets/img/Right button.png";
import left from "../../assets/img/Left button.png";

export default function Testimonial() {
    return (
        <div className="testimonial-section container" id="testimonial">
            <div className="testimonial-content pt-5">
                <center>
                    <h5 className="testimonial-title pt-5">Testimonial</h5>
                    <p>Berbagai review positif dari para pelanggan kami</p>
                </center>
            </div>
            <OwlCarousel
                className="owl-theme"
                loop={true}
                margin={32}
                autoHeight={true}
                nav={true}
                center={true}
                dots={false}
                navText={[
                    `<img src=${left} style='height: 32px;width:32px;'>`,
                    `<img src=${right} style='height: 32px;width:32px;'>`,
                ]}
                responsive={{
                    0: {
                        items: 1,
                    },
                    600: {
                        items: 1,
                    },
                    1000: {
                        items: 2,
                    },
                }}

            >
                <div className="card container">
                    <div className="testimonial-carousel row">
                        <div className="testimonial-img col-md-3 pl-3 pt-5">
                            <img src={photo} class="profile" alt="..." />
                        </div>
                        <div className="testimonial-text col-md-9 my-4">
                            <div className="card-body">
                                <img src={rate} class="star-image" alt="" />
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                                    amet, consectetur adipiscing elit, sed do eiusmod
                                </p>
                                <p className="bold-text">John Dee 32, Bromo</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card mb-1">
                    <div className="testimonial-carousel row">
                        <div className=" testimonial-img col-md-3 pl-3 pt-5">
                            <img src={photo2} class="profile" alt="..." />
                        </div>
                        <div className="testimonial-text col-md-9 my-4">
                            <div className="card-body">
                                <img src={rate} class="star-image" alt="" />
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                                    amet, consectetur adipiscing elit, sed do eiusmod
                                </p>
                                <p className="bold-text">John Dee 32, Bromo</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card mb-1">
                    <div className="testimonial-carousel row">
                        <div className="testimonial-img col-md-3 pl-3 pt-5">
                            <img src={photo} class="profile" alt="..." />
                        </div>
                        <div className="testimonial-text col-md-9 my-4">
                            <div className="card-body">
                                <img src={rate} class="star-image" alt="" />
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                                    amet, consectetur adipiscing elit, sed do eiusmod
                                </p>
                                <p className="bold-text">John Dee 32, Bromo</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="card mb-1">
                    <div className="testimonial-carousel row">
                        <div className=" testimonial-img col-md-3 pl-3 pt-5">
                            <img src={photo2} class="profile" alt="..." />
                        </div>
                        <div className="testimonial-text col-md-9 my-4">
                            <div className="card-body">
                                <img src={rate} class="star-image" alt="" />
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur
                                    adipiscing elit, sed do eiusmod lorem ipsum dolor sit
                                    amet, consectetur adipiscing elit, sed do eiusmod
                                </p>
                                <p className="bold-text">John Dee 32, Bromo</p>
                            </div>
                        </div>
                    </div>
                </div>
            </OwlCarousel >
        </div>
    )
}

