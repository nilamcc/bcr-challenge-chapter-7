import React from "react";
import {Container} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../assets/css/LandingPage.css';
import car from '../../assets/img/img_car.png';

export default function TextMain() {
    return(
        <div className="main-section">
            <Container>
                <div className="main-seaction-text mt-4">
                    <div className="row">
                        <div className="col-md-6">
                            <h1>Sewa & Rental Mobil Terbaik di <br/>kawasan (Lokasimu)</h1>
                            <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
                        </div>
                        <div className="main-seaction-pict container-fluid col-sm-6">
                            <img className="img-fluid float-right mt-4" src={car} width="600px" height="380px" alt=""></img>
                        </div>
                    </div>
                </div>  
            </Container>
        </div>
    )
}