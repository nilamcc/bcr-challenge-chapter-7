import { React, useEffect, useState } from "react";
import FilterCar from "./FilterCar";
import { useSelector, useDispatch } from "react-redux";
import { fetchUsers } from "../../redux"
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../assets/css/LandingPage.css';
import { Container } from 'react-bootstrap';

export default function SearchBar() {
    const [type, setType] = useState("Pilih Tipe Driver");
    const [date, setDate] = useState("Pilih Waktu");
    const [pickupTime, setPickupTime] = useState("8");
    const [passenger, setPassenger] = useState("");
    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    const handleSubmit = (e) => {
        e.preventDefault();
        if (type !== "Pilih Tipe Driver") {
            const pass = passenger ? passenger : "0";
            const filter = { type, date, pickupTime, pass };
            dispatch(fetchUsers(filter));
        }
    };

    const handleType = (e) => {
        setType(e.target.value);
    };
    const handleDate = (e) => {
        setDate(e.target.value);
    };
    const handlePickupTime = (e) => {
        setPickupTime(e.target.value);
    };
    const handlePassenger = (e) => {
        setPassenger(e.target.value);
    };

    useEffect(() => {
        console.log(state.cars);
    }, [state]);


    return (
        <>
        <Container>
            <div class="searchBar container border bg-white rounded p-3 position-relative"  style={{ zIndex: 2 }}>
                <form onSubmit={handleSubmit}>
                    <div class="container row gx-3 gy-2 align-items-center" onClick="activeDarkBackground()">
                        <div class="col-sm-2 pl-5 justify-content-center">
                            <label htmlfor="typeDriver">Tipe Driver</label>
                            <select value={type} required onChange={handleType} class="form-select type" name="typeDriver" id="typeDriver">
                                <option disabled hidden>Pilih Tipe Driver</option>
                                <option value="yes">Dengan Supir</option>
                                <option value="no">Tanpa Supir (Lepas Kunci)</option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <label htmlfor="date">Tanggal</label>
                            <input onChange={handleDate} required type="date" name="date" class="form-control" id="date" />
                        </div>
                        <div class="col-sm-3">
                            <label htmlfor="time">Waktu Jemput/Ambil</label>
                            <select value={pickupTime} onChange={handlePickupTime} className="form-select time" name="time" id="time">
                                <option value="8">08.00 WIB</option>
                                <option value="9">09.00 WIB</option>
                                <option value="10">10.00 WIB</option>
                                <option value="11">11.00 WIB</option>
                                <option value="12">12.00 WIB</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <label htmlfor="passenger">Jumlah Penumpang (Optional)</label>
                            <input value={passenger} onChange={handlePassenger} id="passenger" type="number" class="form-control" name="passenger" placeholder="Jumlah Penumpang" />
                        </div>
                        <div class="col-sm-2 mt-4 d-flex flex-column justify-content-center">
                            <div>
                                <button class="btn btn-success submit" >Cari Mobil</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </Container>
        { state.cars && <FilterCar cars={state.cars} /> }
        </>
    );
};