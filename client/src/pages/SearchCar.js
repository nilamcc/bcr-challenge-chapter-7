import React from 'react'
import Headers from '../components/landing page/Navbar'
import Footers from '../components/landing page/Footer'
import TextMain from '../components/car page/TextMainNoButton'
import SearchBar from '../components/car page/SearchBar'

export default function LandingPage() {
    return (
        <>
            <Headers />
            <TextMain />
            <SearchBar />
            <Footers />
        </>
    )
}