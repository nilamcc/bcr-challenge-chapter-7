import React from 'react'
import Headers from '../components/landing page/Navbar'
import Footers from '../components/landing page/Footer'
import TextMain from '../components/landing page/TextMain'
import OurService from '../components/landing page/OurService'
import WhyUs from '../components/landing page/WhyUs'
import Banner from '../components/landing page/Banner'
import FAQ from '../components/landing page/FAQ'
import Testimonial  from '../components/landing page/Testimonial'

export default function LandingPage() {
    return (
        <>
            <Headers />
            <TextMain />
            <OurService />
            <WhyUs />
            <Testimonial />
            <Banner />
            <FAQ />
            <Footers />
        </>
    )
}